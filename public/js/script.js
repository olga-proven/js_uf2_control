
document.addEventListener("DOMContentLoaded", (event) => {
  console.log("DOM fully loaded and parsed");
  //document.cookie = "visit=1";
  
  checkCookie();
  document.getElementById("animalForm").style.display = "none";
  document.getElementById("list").addEventListener("click", getAnimalList);
  document.getElementById("add").addEventListener("click", showAnimalForm);
  document.getElementById("submitAnimalForm").addEventListener("click", sendAnimalForm);
  document.getElementById("showLocalStorage").addEventListener("click", showInfoLocalStorage);
  activarButtons();


});

function createCookie() {
  var date = new Date();
  date.setTime(date.getTime() + (7 * 24 * 60 * 60 * 1000)); // 1 semana
  var expires = "; expires=" + date.toString();
  document.cookie = "visit=1" + expires;
}



async function getAnimalList() {
  document.getElementById("animalForm").style.display = "none";
  let animallist = await sendToServer("GET", "/getAnimalList", "");
  //console.log(animallist);
  //console.log(animallist[0].id);
  createTable(animallist);

  localStorage.setItem('Animals', JSON.stringify(animallist));
  /*   for (var i = 0; i < animallist.length; i++) {
      console.log(animallist[i]);
      console.log(animallist[i].id);
      console.log(animallist[i].especie);
  
    } */


}

function createTable(animallist) {
  document.getElementById("tableDiv").style.display = "inline";

  columnas = ["Id", "Especie", "Sexe", "Any naixement", "Pais", "Continent", "Delete"]

  table = document.getElementById("tableElement");
  table.innerHTML = "";
  row1 = document.createElement("tr");
  table.appendChild(row1);
  for (var i = 0; i < columnas.length; i++) {
    column = document.createElement("th");
    column.innerHTML = columnas[i];
    row1.appendChild(column);

  }

  for (var i = 0; i < animallist.length; i++) {
    row = document.createElement("tr");
    table.appendChild(row);

    column1 = document.createElement("td");
    column1.innerHTML = animallist[i].id;


    column2 = document.createElement("td");
    column2.innerHTML = animallist[i].especie;

    column3 = document.createElement("td");
    column3.innerHTML = animallist[i].sexe;

    column4 = document.createElement("td");
    column4.innerHTML = animallist[i].any_naixement;

    column5 = document.createElement("td");
    column5.innerHTML = animallist[i].pais;
    column6 = document.createElement("td");
    column6.innerHTML = animallist[i].continent;

    column7 = document.createElement("td");
    button = document.createElement("button");

    column7.appendChild(button);
    button.innerHTML = animallist[i].especie;
    button.id = animallist[i].especie;
    button.classList.add("deleteButton")

    row.appendChild(column1);
    row.appendChild(column2);
    row.appendChild(column3);
    row.appendChild(column4);

    row.appendChild(column5);
    row.appendChild(column6);
    row.appendChild(column7);


  }
  activarButtons()

}

function showAnimalForm() {
  document.getElementById("tableDiv").style.display = "none";
  document.getElementById("animalForm").style.display = "inline";

}

function sendAnimalForm() {
  //console.log("en send animal form");

  let animal = {
    especie: document.getElementById("especie").value,
    sexe: document.getElementById("sexe").value,
    any_naixement: document.getElementById("any").value,
    pais: document.getElementById("pais").value,
    continent: document.getElementById("continent").value
  };
  //console.log(animal);
  putAnimalInDatabase(animal);


  if (putAnimalInDatabase) {
    console.log("Animal added");
    getAnimalList();
  }

}

async function putAnimalInDatabase(animal) {

  let pushData = await sendToServer("POST", "/pushAnimal", animal);
  console.log(pushData);
  if (pushData.result_sql === true) {
    return true;
  } else {
    return false;
  }
}



function showInfoLocalStorage() {

  let info = localStorage.getItem('Animals');
  console.log(info);

}


function activarButtons() {
  buttons = document.getElementsByClassName("deleteButton");
  buttons2 = document.querySelectorAll(".deleteButton");
  console.log(buttons);
  console.log(buttons2);

  for (let i = 0; i < buttons.length; i++) {
    let animalName = buttons[i].innerHTML;
    console.log(animalName);
    buttons[i].addEventListener("click", function () {
      deleteAnimal(animalName);
    });

  }
}

async function deleteAnimal(animalName) {
  let animal = {
    especie: animalName
  };
  console.log(animal);
  let deleteData = await sendToServer("POST", "/deleteAnimal", animal);
  console.log(deleteData);
  getAnimalList();
  if (deleteData.result_sql === true) {
    console.log(deleteData.result_sql);
    getAnimalList();
    return true;
  } else {
    return false;
  }
}


function checkCookie() {
 

  if (getCookieValue("visit") != null && (parseInt(getCookieValue("visit")) <= 10)) {
    let divDeleteCookie = document.getElementById("divDeleteCookie");
    divDeleteCookie.style.display = "none";
    var newCookieValue = 0;
    let cookieValue = parseInt(getCookieValue("visit"));
    console.log(cookieValue);
    newCookieValue = cookieValue + 1;
    document.cookie = "visit=" + newCookieValue.toString();
  }
  else if (getCookieValue("visit") != null && (parseInt(getCookieValue("visit")) > 10)) {
    let divDeleteCookie = document.getElementById("divDeleteCookie");
    divDeleteCookie.style.display = "inline";
    let buttonDeleteCookie = document.getElementById("deleteCookie");
    buttonDeleteCookie.addEventListener("click", function () {
      document.cookie = "visit=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
      divDeleteCookie.style.display = "none";
    });
  } else {
    createCookie();
  }
}




function getCookieValue(cookieName) {

  let cookies = document.cookie;
  let cookieArray = cookies.split("; ");
  for (var i = 0; i < cookieArray.length; i++) {
    let cookie = cookieArray[i];
    let [name, value] = cookie.split("=");

    if (name === cookieName) {
      console.log(name);
      console.log(value);

      return value;
    }
  }

  return null;
}


