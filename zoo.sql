-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-12-2023 a las 11:44:16
-- Versión del servidor: 10.4.28-MariaDB
-- Versión de PHP: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `zoo`
--
CREATE DATABASE IF NOT EXISTS `zoo` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `zoo`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `animals`
--

CREATE TABLE `animals` (
  `id` int(11) NOT NULL,
  `especie` varchar(50) NOT NULL,
  `sexe` varchar(50) NOT NULL,
  `any_naixement` varchar(50) NOT NULL,
  `pais` varchar(50) NOT NULL,
  `continent` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `animals`
--

INSERT INTO `animals` (`id`, `especie`, `sexe`, `any_naixement`, `pais`, `continent`) VALUES
(1, 'lleó', 'mascle', '16/12/2003', 'Angola', 'Àfrica'),
(2, 'Suricata', 'femella', '20/03/2020', 'Botsuana', 'Àfrica'),
(3, 'Cóndor Andí', 'mascle', '11/11/1995', 'Chile', 'América'),
(4, 'Cangur', 'femella', '28/05/2015', 'Australia', 'Oceania'),
(5, 'linx', 'mascle', '07/10/2015', 'Espanya', 'Europa'),
(6, 'Ós Bru', 'femella', '15/04/2010', 'Russia', 'Europa'),
(7, 'Ós Panda', 'mascle', '22/05/2018', 'China', 'Ásia'),
(8, 'Faisà Verd', 'femella', '29/01/2013', 'Japó', 'Ásia');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `animals`
--
ALTER TABLE `animals`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `animals`
--
ALTER TABLE `animals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
